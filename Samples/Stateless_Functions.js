import React, { Component, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
export default class App extends Component {

render() {
 return <StateLessDemo></StateLessDemo>
}

}



const StateLessDemo = () => {
  const [stateColor, setColor] = useState("red"); 
  let colors = ["red", "yellow", "blue", "violet", "rgb(255,120,1)", "salmon"];
  console.log(stateColor);
  return <View
    style={{
      flex: 1,
      flexDirection: "column",
      backgroundColor: stateColor,
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    {colors.map((name, index) => {
    return <PropButton key={index} color={name} callback={()=>setColor(name)}/>
    })}
  </View>
}

const PropButton = ({color,callback}) => {
  return <TouchableOpacity style={{
    width: "90%",
    flexDirection: "row",
    borderWidth: 2,
    borderColor: "#ffffff",
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: "rgba(255,255,255,0.4)",
    margin: 10}} onPress={callback}>  
    <View style={{
    width: 20,
    height: 20,
    borderRadius: 100 / 2,
    borderWidth: 2,
    borderColor: "#ffffff",
    backgroundColor: color,
    margin: 20}}></View>
    <Text style={{
    color: "#000",
    fontSize: 30,
    alignSelf: "center"}}>{color}</Text>
  </TouchableOpacity> 
}