import React, { Component } from 'react';
import { Button, StyleSheet, View, Text, Image } from 'react-native';

export default class Luminous extends Component {
 
  constructor(){
    super();
    this.state={
      on: false,
      source : 'https://www.w3schools.com/js/pic_bulboff.gif',
      switch: 'Turn On',
      color: 'yellow'
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
      style={{width: 100, height: 180}}
      source={{uri:this.state.source}}
    />
        <View style={styles.buttonGreen}>
          <Button
            onPress={this._inversion}
            title={this.state.switch}
            color={this.state.color}
          />
        </View>
      </View>
    );
  }

  _inversion = () => {
    if (this.state.on)
    this.setState({
      on: false,
      source : 'https://www.w3schools.com/js/pic_bulboff.gif',
      switch: 'Turn On',
      color: 'yellow'
    });
    else
    this.setState({
      on: true,
      source : 'https://www.w3schools.com/js/pic_bulbon.gif',
      switch: 'Turn Off',
      color: 'red'
    });
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  buttonGreen: {
    margin: 10,
    justifyContent: "space-between",
    backgroundColor: "#90A4AE",
    color: "#fff"
  },
  buttonRed: {
    margin: 10,
    justifyContent: "space-between",
    backgroundColor: "red",
    color: "#fff"
  },
  buttonSet:{
    margin: 20
  },
  textData: {
    fontSize: 150
  },
});
